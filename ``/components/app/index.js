import React from 'react';
import html from 'react-inner-html';

const PATH = "https://ria.ru/ips/rsv/";
// const PATH = "";

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state={
      wd: window.innerWidth,
      hg: window.innerHeight,
      list1: false,
      list2: false
    }

    this.handleResize = this.handleResize.bind(this);
  }

  componentDidMount() {
    var el = document.getElementById('app-root');
    if(el){
      el.style.visibility = 'visible';
      el.focus();
    }

    // resize
    window.addEventListener('resize', this.handleResize);
    this.handleResize();
    setTimeout(this.handleResize, 1000);
  }

  handleResize() {
    const iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
    const wd = window.innerWidth;
    const hg = window.innerHeight;
    this.setState({wd: wd, hg: hg});
  }

  render() {
    const { data } = this.props;

    const ICONS = [
      `международный инженерный чемпионат «CASE-IN»`,
      `конкурс управленцев «Лидеры России»`,
      `студенческая олимпиада «Я – профессионал»`,
      `международный конкурс «Мой первый бизнес»`,
      `всероссийский конкурс «Доброволец России»`,
      `проект «Профстажировки 2.0»`,
      `фестиваль «Российская студенческая весна»`,
      `«Грантовый конкурс молодежных инициатив»`,
      `конкурс «Цифровой прорыв»`,
      `портал «Бизнес-навигатора МСП»`,
      `конкурс «Лучший социальный проект года»`,
      `всероссийский проект «РДШ-Территория самоуправления»`,
      `соревнования по профессиональному мастерству среди людей с инвалидностью «Абилимпикс»`,
      `всероссийский молодежный кубок по менеджменту «Управляй!»`,
      `российская национальная премия «Студент года»`,
      `движение «Молодые профессионалы» (WorldSkills Russia)`,
      `благотворительный проект «Мечтай со мной»`,
      `всероссийский конкурс «Лига вожатых»`,
      `конкурс «Моя страна — моя Россия»`,
      `«Олимпиада Кружкового движения НТИ.Junior»`,
      `всероссийский конкурс «Учитель будущего»`,
      `всероссийский конкурс «Мастера гостеприимства»`
    ];

    const PARTICIPANTS = [
      {
        "header": "Школьная лига",
        "sheader": "",
        "txt": "школьники 9-11 классов",
        "list": [],
        "years": "до 18 лет",
        "iconname":"school"
      },
      {
        "header": "Лига рабочих специальностей ",
        "sheader": "",
        "txt": "учащиеся профессиональных образовательных организаций ",
        "list": [],
        "years": "до 20 лет",
        "iconname":"technical"
      },
      {
        "header": "Студенческая лига",
        "sheader": "",
        "txt": "студенты и аспиранты вузов по направлениям:",
        "list": ["геологоразведка","горное дело","металлургия","нефтегазовое дело","нефтехимия","цифровой атом","электроэнергетика"],
        "years": "до 25 лет",
        "iconname":"student"
      },
      {
        "header": "Лига молодых специалистов ",
        "sheader": "",
        "txt": "сотрудники компаний ТЭК и МСК",
        "list": [],
        "years": "до 35 лет",
        "iconname":"profi"
      },
      {
        "header": "Специальные лиги",
        "sheader": "Корпоративные чемпионаты «CASE-IN»",
        "txt": "школьники, студенты, молодые сотрудники отраслевых компаний",
        "list": [],
        "years": "до 35 лет",
        "iconname":"special"
      }
    ];

    const TOP5 = [
      {
        "name": "Геологоразведка",
        "vuz": [
          "Уральский государственный горный университет (Екатеринбург)",
          "Санкт-Петербургский горный университет (Санкт-Петербург)",
          "Российский государственный геологоразведочный университет имени Серго Орджоникидзе (Москва)",
          "Национальный исследовательский Томский государственный университет (Томск)",
          "Северо-Восточный государственный университет (Магадан)"
        ]
      },
      {
        "name": "Горное дело",
        "vuz": [
          "Санкт-Петербургский горный университет (Санкт-Петербург)",
          "Уральский государственный горный университет (Екатеринбург)",
          "Кузбасский государственный технический университет имени Т.Ф. Горбачева (Кемерово)",
          "Магнитогорский государственный технический университет им. Г.И. Носова (Магнитогорск)",
          "Северо-Восточный государственный университет (Магадан)"
        ]
      },
      {
        "name": "Металлургия",
        "vuz": [
          "Б.Н. Ельцина (Екатеринбург)",
          "Липецкий государственный технический университет (Липецк)",
          "Новотроицкий филиал НИТУ «МИСиС» (Новотроицк)",
          "Санкт-Петербургский горный университет (Санкт-Петербург)",
          "Сибирский государственный индустриальный университет (Новокузнецк)"
        ]
      },
      {
        "name": "Нефтегазовое дело",
        "vuz": [
          "Тюменский индустриальный университет (Тюмень)",
          "Российский государственный университет нефти и газа имени И. М. Губкина (Москва)",
          "Санкт-Петербургский горный университет (Санкт-Петербург)",
          "Альметьевский государственный нефтяной институт (Альметьевск)",
          "Сибирский федеральный университет (Красноярск)"
        ]
      },
      {
        "name": "Нефтехимия",
        "vuz": [
          "Пермский национальный исследовательский политехнический университет (Пермь)",
          "Самарский государственный технический университет (Самара)",
          "Национальный исследовательский Томский политехнический университет (Томск)",
          "Воронежский государственный университет инженерных технологий (Воронеж)",
          "Амурский государственный университет (Благовещенск)"
        ]
      },
      {
        "name": "Цифровой атом",
        "vuz": [
          "Нижегородский государственный технический университет им. Р.Е.Алексеева (Нижний Новгород)",
          "Новосибирский государственный технический университет (Новосибирск)",
          "Иркутский национальный исследовательский технический университет (Иркутск)",
          "Тюменский индустриальный университет (Тюмень)",
          "Санкт-Петербургский политехнический университет Петра Великого (Санкт-Петербург)"
        ]
      },
      {
        "name": "Электроэнергетика",
        "vuz": [
          "Самарский государственный технический университет (Самара)",
          "Национальный исследовательский Томский политехнический университет (Томск)",
          "Ивановский государственный энергетический университет имени В.И. Ленина (Иваново)",
          "Уральский федеральный университет имени первого Президента России Б.Н. Ельцина (Екатеринбург)",
          "Санкт-Петербургский горный университет (Санкт-Петербург)"
        ]
      },
      {
        "name": "Лига молодых специалистов",
        "vuz": [
          "ПАО «Татнефть»",
          "АО «СО ЕЭС»",
          "ПАО «ФСК ЕЭС»",
          "ПАО «РОссети»",
          "ПАО «МРСК Северо-Запада» (входит в группу компаний ПАО «Россети»)"
        ]
      }
    ];

    const ROME = ["I", "II", "III", "IV", "V"]

    const VUZ_SCHOOL = [
      "Выксунский филиал Национального исследовательского технологического университета «МИСиС»",
      "Ивановский государственный энергетический университет имени В.И. Ленина",
      "Иркутский национальный исследовательский технический университет",
      "Национальный исследовательский технологический университет «МИСиС»",
      "Национальный исследовательский Томский государственный университет",
      "Национальный исследовательский Томский политехнический университет",
      "Национальный исследовательский университет «МЭИ",
      "Новосибирский государственный технический университет",
      "Омский государственный технический университет",
      "Российский университет дружбы народов",
      "Санкт-Петербургский политехнический университет Петра Великого",
      "Северо-Восточный федеральный университет имени М. К. Аммосова",
      "Северо-Кавказский федеральный университет",
      "Сибирский государственный индустриальный университет",
      "Старооскольский филиал Национального исследовательского технологического университета «МИСиС»",
      "Тольяттинский государственный университет",
      "Тульский государственный университет",
      "Филиал Национального исследовательского университета «МЭИ»» в г. Волжском"
    ]

    const VUZ_HIGH = [
      "Национальный исследовательский Томский политехнический университет",
      "Уральский федеральный университет имени первого Президента Б.Н. Ельцина",
      "Пермский национальный исследовательский технический университет",
      "Национальный исследовательский университет «МЭИ»",
      "Новосибирский государственный технический университет",
      "Северо-Восточный федеральный университет имениМ. К. Аммосова",
      "Иркутский национальный исследовательский технический университет"
    ]

    const REGIONS = [
      "Центральный федеральный округ",
      "Северо-Западный федеральный округ",
      "Южный федеральный округ",
      "Северо-Кавказский федеральный округ",
      "Приволжский федеральный округ",
      "Уральский федеральный округ",
      "Сибирский федеральный округ",
      "Дальневосточный федеральный округ"
    ]

    const ORG = [
      "Фонд «Надежная смена»",
      "Молодежный форум лидеров горного дела",
      "ООО «АстраЛогика»",
      "Президентская платформа «Россия – страна возможностей»"
    ]

    return (
      <div id="casein">


        <section id="folder">
          <img id="folder-bg" src={`${PATH}img/image-1.jpg`}/>
          <div id="folder-container">
            <div id="folder-header">
                <div id="folder-header-left">
                    <img id="folder-header-left-img" src={`${PATH}img/logo-blue-asset-2.svg`}/>
                </div>
                <div id="folder-header-right">
                    <img id="folder-header-right-img" src={`${PATH}img/logo-rossiya-strana-copy.svg`}/>
                </div>
            </div>
            <div id="folder-divider" />
            <div id="folder-text">{`Международный инженерный чемпионат «CASE-IN» входит в линейку проектов президентской платформы «Россия — страна возможностей». CASE-IN — это международная система соревнований по решению инженерных кейсов среди студентов, школьников и молодых специалистов. За время своего существования Чемпионат стал эффективны социальным лифтом для молодежи и кузницей кадров для крупнейших предприятий России`}</div>
          </div>
        </section>

        <div className="divider-blue"/>

        {/*************/}

        <div className="divider"/>

        <section id="block0">
          <img 
            id="puzzle"
            src={`${PATH}img/puzzle.png`}
            srcSet={`${PATH}img/puzzle@2x.png 2x, ${PATH}img/puzzle@3x.png 3x`}
          />
          <div
            id="block0-text"
          >
            <span className="block-text">{`Международный инженерный чемпионат `}</span>
            <span className="block-text-blue">{`«CASE-IN»`}</span>
            <span className="block-text">{` нацелен на популяризацию инженерно-технического образования и привлечение перспективных молодых специалистов в топливно-энергетический (ТЭК) и минерально-сырьевой комплексы (МСК)`}</span>
          </div>
        </section>

        <section id="block1">
          <div className="block1-item" data-id={`0`}>
            <img className="block1-item-icon" src={`${PATH}img/uchastnik.svg`}/>
            <div className="block1-item-texts">
              <div className="block1-item-value">{`20 000`}</div>
              <div className="block1-item-text">{`участников*`}</div>
            </div>
          </div>

          <div className="block1-item" data-id={`1`}>
            <img className="block1-item-icon" src={`${PATH}img/partners.svg`}/>
            <div className="block1-item-texts">
              <div className="block1-item-value">{`170`}</div>
              <div className="block1-item-text">{`партнёров`}</div>
            </div>
          </div>

          <div className="block1-item" data-id={`2`}>
            <img className="block1-item-icon" src={`${PATH}img/university.svg`}/>
            <div className="block1-item-texts">
              <div className="block1-item-value">{`60`}</div>
              <div className="block1-item-text">{`вузов`}</div>
            </div>
          </div>

          <div className="block1-item" data-id={`3`}>
            <img className="block1-item-icon" src={`${PATH}img/expert.svg`}/>
            <div className="block1-item-texts">
              <div className="block1-item-value">{`4 200`}</div>
              <div className="block1-item-text">{`экспертов`}</div>
            </div>
          </div>

          <div className="block1-item" data-id={`4`}>
            <img className="block1-item-icon" src={`${PATH}img/location.svg`}/>
            <div className="block1-item-texts">
              <div className="block1-item-value">{`40`}</div>
              <div className="block1-item-text">{`регионов`}</div>
            </div>
          </div>

          <div id="block1-star">{`*С 2013 года`}</div>
        </section>
        
        <div className="divider"/>


        {/*************/}

        <section className="header">
          <span className="header-blue">{`/`}</span>
          <span className="header-bold">{`Организаторы`}</span>
        </section>
        
        <div className="divider"/>
        
        <section id="block19">
            <div className="block19-item" key={0}>
                <div className="block19-imgcontainer">
                  <img className="block19-item-img" src={`${PATH}img/smena.png`} srcSet={`${PATH}img/smena@2x.png 2x, ${PATH}img/smena@3x.png 3x`}/>
                </div>
                <div className="block19-item-txt">{ORG[0]}</div>
            </div>
            <div className="block19-item" key={1}>
                <div className="block19-imgcontainer">
                  <img className="block19-item-img" src={`${PATH}img/forum-gornogog-dela.png`} srcSet={`${PATH}img/forum-gornogog-dela@2x.png 2x, ${PATH}img/forum-gornogog-dela@3x.png 3x`}/>
                </div>
                <div className="block19-item-txt">{ORG[1]}</div>
            </div>
            <div className="block19-item" key={2}>
                <div className="block19-imgcontainer">
                  <img className="block19-item-img" src={`${PATH}img/astologika.png`} srcSet={`${PATH}img/astologika@2x.png 2x, ${PATH}img/astologika@3x.png 3x`}/>
                </div>
                <div className="block19-item-txt">{ORG[2]}</div>
            </div>
            <div className="block19-item" key={3}>
                <div className="block19-imgcontainer">
                  <img className="block19-item-img" src={`${PATH}img/logo-rossiya-strana-copy-2.png`} srcSet={`${PATH}img/logo-rossiya-strana-copy-2@2x.png 2x, ${PATH}img/logo-rossiya-strana-copy-2@3x.png 3x`}/>
                </div>
                <div className="block19-item-txt">{ORG[3]}</div>
            </div>
        </section>

        <div className="divider"/>

        {/*************/}

        <section className="header">
          <span className="header-blue">{`/`}</span>
          <span className="header-bold">{`Лиги Чемпионата`}</span>
        </section>

        <div className="divider"/>

        <section id="block6">
          {
              PARTICIPANTS.map((d,i) => (

                <div key={i} className="block6-item" data-id={i}>
                  <img className="block6-item-icon" src={`${PATH}img/${d.iconname}.png`} srcSet={`${PATH}img/${d.iconname}@2x.png 2x, ${PATH}img/${d.iconname}@3x.png 3x`}/>
                  
                  <div className="block6-item-header-container">
                    <div className="block6-item-header">{d.header}</div>
                    <div className="block6-item-sheader">{d.sheader}</div>
                  </div>

                  <div className="block6-item-line" />
                  <div className="block6-item-txt">{d.txt}</div>
                  <ul className="block6-ul">
                    {
                      d.list.map((q,j)=>(
                        <li key={j} className="block6-li">{q}</li>
                      ))
                    }
                  </ul>
                  <div className="block6-item-years">{d.years}</div>
                </div>

              ))
          }
        </section>

        <div className="divider"/>

        {/*************/}

        <section className="header">
          <span className="header-blue">{`/`}</span>
          <span className="header-bold">{`Этапы Чемпионата`}</span>
        </section>

        <div className="divider"/>

        <section id="block8">
          <div id="block8-left">
            <div id="block8-el0">{`В рамках конкурса участники решают кейсы по самым актуальным задачам, стоящим перед крупнейшими предприятиями России, а затем представляют свои идеи экспертной комиссии`}</div>
            
            <div id="block8-el1">
              <img id="block8-el1-img0" src={`${PATH}img/2.png`} srcSet={`${PATH}img/2@2x.png 2x, ${PATH}img/2@3x.png 3x`}/>
              
              <img id="block8-el1-img2" src={`${PATH}img/letter.png`} srcSet={`${PATH}img/letter@2x.png 2x, ${PATH}img/letter@3x.png 3x`}/>
            </div>

            <div className="block8-btn" id="block8-el2"><span>{`Инженерный кейс`}</span></div>
            <div id="block8-el3"><span>{`практическая задача (проблема), основанная на реальной (или максимально приближенной к реальной) производственной ситуации. Инженерный кейс готовится по материалам конкретного предприятия или организации.`}</span></div>
            <div id="block8-items">

              <div key={0} className="block8-item" id="block8-item0">
                <div className="block8-item-value">{`10 дней`}</div>
                <div className="block8-item-txt">{`даётся на решение кейса`}</div>
              </div>

              <div key={1} className="block8-item" id="block8-item1">
                <div className="block8-item-value">{`3-4 человека`}</div>
                <div className="block8-item-txt">{`состав команды в зависимости от лиги`}</div>
              </div>

              <div key={2} className="block8-item" id="block8-item2">
                <div className="block8-item-value">{`6 минут`}</div>
                <div className="block8-item-txt">{`даётся на презентацию`}</div>
              </div>
            </div>
          </div>
          <div id="block8-right">
            <img id="block8-el4" src={`${PATH}img/3.png`} srcSet={`${PATH}img/3@2x.png 2x, ${PATH}img/3@3x.png 3x`}/>
            <div className="block8-btn" id="block8-el5"><span>{`Экспертная комиссия`}</span></div>
            <div id="block8-el6"><span>{`состоит из представителей̆ федеральных и региональных органов власти, предприятий ТЭК и МСК, вузов, отраслевых экспертов`}</span></div>
            <div key={3} className="block8-item" id="block8-item3">
              <div className="block8-item-value">{`3-5 вопросов`}</div>
              <div className="block8-item-txt">{`могут задать члены экспертной комиссии`}</div>
            </div>
            <img id="block8-el7" src={`${PATH}img/4.png`} srcSet={`${PATH}img/4@2x.png 2x, ${PATH}img/4@3x.png 3x`}/>
          </div>
        </section>

        <div className="divider"/>

         {/*************/}

        <section className="header">
          <span className="header-blue">{`/`}</span>
          <span className="header-bold">{`Что получают победители`}</span>
        </section>

        <div className="divider"/>

        <section id="block9">
          <div id="block9-icons">
            <img className="block9-icon" id="block9-icon0" src={`${PATH}img/winers/school.png`} srcSet={`${PATH}img/winers/school@2x.png 2x, ${PATH}img/winers/school@3x.png 3x`}/>
            <img className="block9-icon" id="block9-icon1" src={`${PATH}img/winers/technical.png`} srcSet={`${PATH}img/winers/technical@2x.png 2x, ${PATH}img/winers/technical@3x.png 3x`}/>
            <img className="block9-icon" id="block9-icon2" src={`${PATH}img/winers/student.png`} srcSet={`${PATH}img/winers/student@2x.png 2x, ${PATH}img/winers/student@3x.png 3x`}/>
            <img className="block9-icon" id="block9-icon3" src={`${PATH}img/winers/profi.png`} srcSet={`${PATH}img/winers/profi@2x.png 2x, ${PATH}img/winers/profi@3x.png 3x`}/>
            <img className="block9-icon" id="block9-icon4" src={`${PATH}img/winers/special.png`} srcSet={`${PATH}img/winers/special@2x.png 2x, ${PATH}img/winers/special@3x.png 3x`}/>
          </div>
          <div className="block9-text">{`Профессиональное и личностное развитие, социальный лифт`}</div>
        </section>

        <div className="divider"/>

        <section id="block10">
          <div id="block10-icons">
            <img className="block10-icon" id="block10-icon0" src={`${PATH}img/winers/school.png`} srcSet={`${PATH}img/winers/school@2x.png 2x, ${PATH}img/winers/school@3x.png 3x`}/>
          </div>
          <div className="block10-right">
            <div className="block10-text">
              <ul>
                <li>{`Путёвка на тематическую смену «#ВместеЯрче» в ВДЦ «Орленок»`}</li>
                <li>{`Дополнительные баллы ЕГЭ при поступлении в вузы–партнеры`}</li>
              </ul>
              <img id="block10-img" src={`${PATH}img/invalid-name1.png`} srcSet={`${PATH}img/invalid-name1@2x.png 2x, ${PATH}img/invalid-name1@3x.png 3x`}/>
            </div>
            <div className="block10-list">
              <div className="block10-list-btn"
                onClick={()=>(this.setState({list1: !this.state.list1}))}
              >
                <div>{`Список вузов (на момент написания материала)`}</div>
                <div className="circle-"><span>{`${((this.state.list1)?"-":"+")}`}</span></div>
              </div>
              <div className="block10-list-container" style={{height: (this.state.list1)?"auto":"0"}}>
              {
                VUZ_SCHOOL.map((d,i) => (
                  <div key={i} className="block10-list-item" data-id={i}>{d}</div>
                ))
              }
              </div>
            </div>
          </div>
        </section>

        <div className="divider"/>

        <section id="block11">
          <div id="block11-icons">
            <img className="block10-icon" id="block10-icon0" src={`${PATH}img/winers/student.png`} srcSet={`${PATH}img/winers/student@2x.png 2x, ${PATH}img/winers/student@3x.png 3x`}/>
          </div>
          <div className="block10-right">
            <div className="block10-text">
            <ul>
            <li>{`Стажировки, практики, трудоустройство в ведущие отраслевые компании`}</li>
            <li>{`Включение в потенциальный кадровый резерв, в группы целевого обучения компании в вузе`}</li>
            <li>{`Членство  в Молодежной секции Российского национального комитета СИГРЭ`}</li>
            <li>{`Включение в систему адъюнктов при Академии горных наук`}</li>
            <li>{`Приоритетное право на поступление в магистратуру и аспирантуру`}</li>
            </ul>
            </div>
            <div className="block10-list">
              <div className="block10-list-btn"
                onClick={()=>(this.setState({list2: !this.state.list2}))}
              >
                <div>{`Список вузов (на момент написания материала)`}</div>
                <div className="circle-"><span>{`${((this.state.list2)?"-":"+")}`}</span></div>
              </div>
              <div className="block10-list-container" style={{height: (this.state.list2)?"auto":"0"}}>
              {
                VUZ_HIGH.map((d,i) => (
                  <div key={i} className="block11-list-item" data-id={i}>{d}</div>
                ))
              }
              </div>
            </div>
          </div>
        </section>

        <div className="divider"/>

        <section id="block12">
          <div id="block12-icons">
            <img className="block10-icon" id="block12-icon0" src={`${PATH}img/winers/profi.png`} srcSet={`${PATH}img/winers/profi@2x.png 2x, ${PATH}img/winers/profi@3x.png 3x`}/>
          </div>
          <div className="block10-right">
            <div className="block10-text">
              <ul>
                <li>{`Участие в Международном форуме «Российская энергетическая неделя» и иных федеральных форумах`}</li>
                <li>{`Профессиональный и карьерный рост в своей компании`}</li>
                <li>{`Участие в федеральных отраслевых образовательных проектах`}</li>
              </ul>
            </div>
          </div>
        </section>

        <div className="divider"/>
        <div className="divider"/>
        <div className="divider"/>

         {/*************/}

        <section className="header" id="short-header">
          <span className="header-blue">{`/`}</span>
          <span className="header-bold">{`От студенческого отраслевого конкурса по горному делу — к международному соревнованию мирового класса`}</span>
        </section>

        <div className="divider"/>

        <section id="block13">
          <img className="block13-img" src={`${PATH}img/5.png`} srcSet={`${PATH}img/5@2x.png 2x, ${PATH}img/5@3x.png 3x`}/>
          <div className="block13-bg"></div>
          <div className="block13-line">
            <div className="block13-img-container">
              <img className="block13-line-img" src={`${PATH}img/group-18.png`} srcSet={`${PATH}img/group-18@2x.png 2x, ${PATH}img/group-18@3x.png 3x`}/>
            </div>
            <div className="block13-line-txt">{`Проект реализуется в соответствии с Планом мероприятий, направленных на популяризацию рабочих и инженерных профессий, утвержденным распоряжением Правительства РФ от 5 марта 2015 года №366-р.`}</div>
          </div>
          <div className="block13-line">
            <div className="block13-img-container">
              <img className="block13-line-img" src={`${PATH}img/logotype-fpg.png`} srcSet={`${PATH}img/logotype-fpg@2x.png 2x, ${PATH}img/logotype-fpg@3x.png 3x`}/>
            </div>
            <div className="block13-line-txt">{`С 2018 года Чемпионат реализуется с использованием гранта Президента Российской Федерации на развитие гражданского общества, предоставленного Фондом президентских грантов.`}</div>
          </div>
        </section>

        <div className="divider"/>
  
        <section id="block14">
          <div id="block14-header">{`Проект входит:`}</div>
          <div className="block14-line">
            <div className="block14-img-container">
              <img className="block14-line-img" src={`${PATH}img/raex.png`} srcSet={`${PATH}img/raex@2x.png 2x, ${PATH}img/raex@3x.png 3x`}/>
            </div>
            <div className="block14-line-txt">{`в ТОП – 15 олимпиад мира по версии рейтингового агентства RAEX (РАЭКС-Аналитика)`}</div>
          </div>
          <div className="block14-line">
            <div className="block14-img-container" id="block14-img-container1">
              <img className="block14-line-img" src={`${PATH}img/prjct/group-7.png`} srcSet={`${PATH}img/prjct/group-7@2x.png 2x, ${PATH}img/prjct/group-7@3x.png 3x`}/>
              <img className="block14-line-img" src={`${PATH}img/prjct/image.png`} srcSet={`${PATH}img/prjct/image@2x.png 2x, ${PATH}img/prjct/image@3x.png 3x`}/>
              <img className="block14-line-img" src={`${PATH}img/prjct/group-11.png`} srcSet={`${PATH}img/prjct/group-11@2x.png 2x, ${PATH}img/prjct/group-11@3x.png 3x`}/>
              <img className="block14-line-img" src={`${PATH}img/prjct/logo-rsv.png`} srcSet={`${PATH}img/prjct/logo-rsv@2x.png 2x, ${PATH}img/prjct/logo-rsv@3x.png 3x`}/>
              <img className="block14-line-img" src={`${PATH}img/prjct/group-14.png`} srcSet={`${PATH}img/prjct/group-14@2x.png 2x, ${PATH}img/prjct/group-14@3x.png 3x`}/>
            </div>
            <div className="block14-line-txt">{`в План федеральных молодежных мероприятий, направленных на популяризацию топливно-энергетического комплекса, энергосбережения и инженерно-технического образования на 2020 г., утвержденный Министерством энергетики России, Министерством высшего образования и науки России, Федеральным агентством по делам молодежи, Министерством просвещения России и АНО «Россия — страна возможностей»`}</div>
          </div>
          <div className="block14-line">
            <div className="block14-img-container">
              <img className="block14-line-img" src={`${PATH}img/np.png`} srcSet={`${PATH}img/np@2x.png 2x, ${PATH}img/np@3x.png 3x`}/>
            </div>
            <div className="block14-line-txt">{`в федеральный проект «Социальные лифты для каждого», который является частью национального проекта «Образование»`}</div>
          </div>
        </section>

        <div className="divider"/>

        <section id="block15">
          <div id="block15-header">{`Итоги 2013-2019 гг.`}</div>
          <div id="info">
            <img className="info" src={`${PATH}img/groupi.png`} srcSet={`${PATH}img/groupi@2x.png 2x, ${PATH}img/groupi@3x.png 3x`}/>

          </div>
        </section>

        <div className="divider"/>

        <section id="block16">
          <img 
            id="block16-img"
            src={`${PATH}img/analizing-games-2-4-x.png`}
            srcSet={`${PATH}img/analizing-games-2-4-x@2x.png 2x, ${PATH}img/analizing-games-2-4-x@3x.png 3x`}
          />
          <div
            id="block16-text"
          >
            <span className="block-text16">{`Каждый сезон «CASE-IN» посвящен одной из масштабных тем, актуальных для экономики России: «Развитие Арктики», «Цифровая трансформация» и т. д.`}</span>
          </div>
        </section>

        <section id="block17">
          <div id="block17-left">
            <div id="block17-left-header">{`География`}</div>
            <div id="block17-list">
            {
              REGIONS.map((d,i) => (
                <div className="block17-list-block">
                  <div className="block17-list-dot" data-id={i}></div>
                  <div className="block17-list-item" data-id={i}>{d}</div>
                </div>
              ))
            }
            </div>
          </div>
          <div id="block17-right">
            <img className="block1-item-icon" src={`${PATH}img/map-of-russia-central-federal-district-2018-composition.svg`}/>
          </div>
        </section>

        <div className="divider"/>



         {/*************/}

        <section className="header">
          <span className="header-blue">{`/`}</span>
          <span className="header-bold">{`ТОП 5 вузов и компаний – участников Чемпионата`}</span>
        </section>

        <div className="divider"/>

        <section id="block7">
          {
              TOP5.map((d,i) => (
                <div>
                  <div key={i} className="block7-item" data-id={i}>
                    <div className="block7-left">
                      <div className="block7-name">{d.name}</div>
                      <img className="block7-image" src={`${PATH}img/top5/icon${i}.png`} srcSet={`${PATH}img/top5/icon${i}@2x.png 2x, ${PATH}img/top5/icon${i}@3x.png 3x`}/>
                    </div>
                    <div className="block7-right">
                      {
                        d.vuz.map((q,j) => (
                          <div key={j} className="block7-vuz">
                            <div className="block7-vuz-number"><span>{ROME[j]}</span></div>
                            <div className="block7-vuz-name">{q}</div>
                          </div>
                        ))
                      }
                    </div>
                  </div>

                  <div className="divider"/>
                </div>
              ))
          }
        </section>

        <div 
          id="upbtn" 
          onClick={() => {
            
          }}
        >
          <span>{`Наверх`}</span>
        </div>

        <div className="divider"/>












        <section id="block2">
          <div id="block2-sblock0">
            <span className="block2-sblock0-item">{`БФ «Надёжная смена»`}</span>
            <span className="block2-sblock0-item">{`НП «Молодежный форум лидеров горного дела»`}</span>
            <span className="block2-sblock0-item">{`АНО «Россия – страна возможностей»`}</span>
            <span className="block2-sblock0-item">{`ООО «АстраЛогика»`}</span>
          </div>
          <div id="block2-sblock1">
            <div className="block2-sblock1-item">
              <div className="block2-sblock1-item-header">{`Национальные партнеры Чемпионата`}</div>
              <div className="block2-sblock1-item-text">{`Министерство энергетики РФ, Министерство науки и высшего образования РФ, Министерство природных ресурсов и экологии РФ, Министерство труда и социальной защиты РФ, Министерство промышленности и торговли РФ, а также Федеральное агентство по делам молодежи (Росмолодежь) и Агентство стратегических инициатив по продвижению новых проектов.`}</div>
            </div>
            <div className="block2-sblock1-item">
              <div className="block2-sblock1-item-header">{`Компании − партнёры`}</div>
              <div className="block2-sblock1-item-text">{`АО «СО ЕЭС», ПАО АК «АЛРОСА», ПАО «Татнефть», ООО «Транснефтьэнерго», ПАО «СИБУР Холдинг», ГК «РОСАТОМ», ПАО «ФСК ЕЭС», АО «МХК «ЕвроХим», АО «Росгеология», ООО «Майкромайн Рус», ПАО «НЛМК», АО «Сибирский Антрацит», Объединённая компания «РУСАЛ», АО «Русская медная компания», ООО «ЕвразХолдинг», ООО «Распадская Угольная Компания», ООО «Сибирская генерирующая компания», ООО «КОМПАНИЯ «КРЕДО-ДИАЛОГ», ПАО «РусГидро»,  ООО «Прософт-Системы», ООО «Восточная Горнорудная Компания», ПАО «НК «Роснефть», АО «СУЭК»,  ООО «ВГК», ПАО «Т Плюс», ООО «Распадская Угольная Компания», Выксунский металлургический завод ОМК и другие.`}</div>
            </div>
          </div>
        </section>

        <div className="divider"/>

        <section id="block3">
          <div
            id="block3-text"
          >
            <span className="block-text-blue">{`«CASE-IN»`}</span>
            <span className="block-text">{` с 2019 года является частью президентской платформы `}</span>
            <a className="block-text-blue" href="#">{`«Россия – страна возможностей»`}</a>
            <span className="block-text">{`, объединяющей `}</span>
            <span className="block-text-blue">{`20 проектов`}</span>
            <span className="block-text">{` для повышения социальной мобильности и самореализации граждан`}</span>
          </div>
          <img 
            id="logo-rossiya-strana"
            src={`${PATH}img/logo-rossiya-strana.svg`}
          />
        </section>

        <div className="divider"/>

        <section id="block4">
          {
              ICONS.map((d,i) => (

                <div key={i} className="block4-item" data-id={i}>
                  <img className="block4-item-icon" src={`${PATH}img/logo/logo${i}.png`} srcSet={`${PATH}img/logo/logo${i}@2x.png 2x, ${PATH}img/logo/logo${i}@3x.png 3x`}/>
                  <div className="block4-item-text">{d}</div>
                </div>

              ))
          }
        </section>

         <section id="block5">
            <img 
                id="block5-image" 
                src={`${PATH}img/group-13.png`} srcSet={`${PATH}img/group-13@2x.png 2x, ${PATH}img/group-13@3x.png 3x`}/>
        </section>

        <section id="block18">
            <div id="block18-header">{`С 2013 года вузам – участникам Чемпионата присуждается специальная награда «Энергия образования» за организацию лучшего отборочного этапа «CASE-IN»`}</div>
            <div id="block18-container">
                <div className="block18-item">
                    <div className="block18-value">{`540`}</div>
                    <div className="block18-txt">{`финалистов`}</div>
                </div>
                <div className="block18-item">
                    <div className="block18-value">{`57`}</div>
                    <div className="block18-txt">{`ведущих технических вузов России, Белоруссии и Казахстана`}</div>
                </div>
                <div className="block18-item">
                    <div className="block18-value">{`110`}</div>
                    <div className="block18-txt">{`отборочных этапов`}</div>
                </div>
                <div className="block18-item">
                    <div className="block18-value">{`40`}</div>
                    <div className="block18-txt">{`компаний ТЭК и МСК`}</div>
                </div>
            </div>
        </section>

        <div className="divider"/>

      </div>
    );
  }
}
