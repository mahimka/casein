import React from 'react'
import ReactDOM from 'react-dom'
import appInject from './app-inject'
import { connect, event } from './google-tag-manager'

connect({
    project:'rsv',
    version:'scroll'
})

event('entrance')

appInject((container, opts)=>{
    // render react thing to the container 
    ReactDOM.render(<App/> container)
})