const trimTrailingSlashes = s => s.replace(/^(.+?)\/*?$/, "$1")
const trimPathFilename = s => s.replace(/[^\/]*$/, "")
const pathRHead = s => s.split('/').slice(-1).join() // path's reverse head (last part)
const pathRTail = s => s.split('/').slice(0,-1).join('/') // path's reverse tail (all except the last part)
const parseQueryParams = s => s.split('&').map(p=>p.split('=')).reduce((a, v)=>( a[v[0]] = decodeURIComponent(v[1]), a), {})
const getParamsFromUrl =  url => parseQueryParams( url ? (url.split('?')[1])||'' : location.search.slice(1) )

const findAncestor = (e, p)=>{
    while(e){
        const r = p(e)
        if(r)
            return r
      e = e.parentElement
    }
}

const findArr = (e, p)=>{
    for (let i = 0; i < e.length; i++) {
        const r = p(e[i])
        if(r)
            return r
    }
}

const strIncludes = (s, ss) => s.indexOf(ss) > -1

export default (render, opts) => {

    const currentScript = 
        findArr(document.querySelectorAll('script[data-uid]'), 
            s => strIncludes(s.src, 'ria.ru/ips/')&&!s.getAttribute('data-ips-bound') ? s : null)
        || document.currentScript 

    // console.log('currentScript', currentScript)

    if(currentScript)
        currentScript.setAttribute('data-ips-bound', true)

    const containerUid = currentScript && (currentScript.getAttribute('data-uid')||getParamsFromUrl(currentScript.src)['uid'])
    const containerShape = findAncestor(currentScript, e => strIncludes(e.className, 'mod-shape-') ? findArr(e.classList, c=>c.startsWith('mod-shape-') ? c : null) : null) || 'none'
    const layoutArticle = findAncestor(currentScript, e => e.classList.contains('layout-article') ? e : null)
    const injectAsInfographics = findAncestor(currentScript, e => e.classList.contains('article__infographics'))
    const endlessItem = findAncestor(currentScript, e => e.classList.contains('endless__item') ? e : null)
    const scriptName = currentScript && pathRHead(currentScript.src.split('?')[0].split('#')[0])

    const publicPath = currentScript ? pathRTail(currentScript.src) : (location.origin + trimTrailingSlashes(trimPathFilename(location.pathname)))
    // console.log('appInject:publicPath', publicPath)

    if(!window['__ips_app_location__'])
        window.__ips_app_location__ = publicPath

    // TODO: solve this somehow for multiple apps on one page ( it is only important for dynamic import() )
    window.__webpack_public_path__ = publicPath + '/'

    const rootElt = (currentScript && currentScript.parentElement) || document.querySelector('#ips-root') || document.body

    const container = document.createElement('div')
    // ips-app-container.className = 'ips-app-container'
    rootElt.appendChild(container)

    const xopts = {
        currentScript,
        publicPath,
        scriptName,
        project: pathRHead(trimTrailingSlashes(publicPath)),
        containerUid,
        containerShape,
        layoutArticle,
        injectAsInfographics,
        endlessItem,
        ...opts
    }
    console.log('mounting app', xopts, 'at', container)
    render(container, xopts)
}
