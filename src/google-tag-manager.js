"use strict";

// Object.defineProperty(exports, "__esModule", {
//   value: true
// });
// exports.connect = connect;
// exports.post = post;
// exports.event = event;

var dataLayerName = 'dataLayer';
var dataLayer = window[dataLayerName] || [];
window[dataLayerName] = dataLayer;

(function (w, d, l, i) {
  w[l] = w[l] || [];
  w[l].push({
    'gtm.start': new Date().getTime(),
    event: 'gtm.js'
  });
  var u = 'https://www.googletagmanager.com',
      s = 'script',
      f = d.getElementsByTagName(s)[0],
      j = d.createElement(s),
      k = d.createElement('noscript'),
      m = d.createElement('iframe'),
      dl = l != 'dataLayer' ? '&l=' + l : '';
  j.async = true;
  j.src = u + '/gtm.js?id=' + i + dl;
  m.src = u + '/ns.html?id=' + i;
  m.height = "0";
  m.width = "0";
  m.style.cssText = "display:none;visibility:hidden";
  k.appendChild(m);
  f.parentNode.insertBefore(j, f);
  f.parentNode.insertBefore(k, j);
})(window, document, dataLayerName, 'GTM-T2NZ6V');

var defApp = {
  location: {}
};
var app = defApp;
var generalOpts = {};

export function connect(newApp) {
  var o = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
  app = newApp || defApp;
  generalOpts = o;
}

export function post(event) {
  // trace('gtm post', event);
  dataLayer.push(event);
}

export function event(event) {
  var opts = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  try {
    var slideName = ''; //slideName = app.location.slide || '';

    var curAnchor = ''; //app.location.anchor || '';

    var dlEvent = opts;
    dlEvent.event = event; // dlEvent.anchor = curAnchor;

    dlEvent.project = app.project || app.projectName || 'noname';
    dlEvent.version = app.version || app.appMode;
    if (generalOpts.log) trace('gtm.event', dlEvent);

    if (app.router) {
      dlEvent.from = app.router.getState().path;
    }

    if (event != 'pageview' && 'undefined' != typeof para) dlEvent[event] = para;

    if (event == 'generic') {
      dlEvent.object_id = para2;
    }

    post(dlEvent);
  } catch (err) {
    error(err);
  }
}