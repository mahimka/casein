import './index.css';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/app';
import data from 'data/data.json';

const root = document.getElementById('app-root');

root.setAttribute('data-ua', navigator.userAgent);
ReactDOM.render(<App data={data} />, root);




// import './index.css';

// import React from 'react'
// import ReactDOM from 'react-dom'
// import App from './components/app';
// import data from 'data/data.json';
// import appInject from './app-inject'
// import { connect, event } from './google-tag-manager'

// connect({
//     project:'rsv',
//     version:'scroll'
// })

// event('entrance')

// appInject((container, opts)=>{
//     // render react thing to the container 
//     ReactDOM.render(<App/> ,container)
// })