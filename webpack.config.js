const PROD = process.env.NODE_ENV === 'production';
const DEV = process.env.NODE_ENV !== 'production';
const PUBLIC_URL = PROD ? require('./package.json').publicUrl : '';

const path = require('path');
const webpack = require('webpack');

const ModuleScopePlugin = require('react-dev-utils/ModuleScopePlugin');
const InterpolateHtmlPlugin = require('react-dev-utils/InterpolateHtmlPlugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin');
const WatchMissingNodeModulesPlugin = require('react-dev-utils/WatchMissingNodeModulesPlugin');
const WebpackChunkHash = require('webpack-chunk-hash');
const {BundleAnalyzerPlugin} = require('webpack-bundle-analyzer');

module.exports = {
  context: path.resolve(__dirname, 'src'),
  
  entry: {
    build: 'index.js',
  },

  output: {
    path: path.resolve(__dirname, 'www'),
    filename: '[name].js',
    chunkFilename: '[name].chunk.js',
    publicPath: PUBLIC_URL
  },

  resolve: {
    // Look for files in these directories
    modules: [
      path.resolve(__dirname, 'node_modules'),
      path.resolve(__dirname, 'src'),
    ],
    plugins: [
      // Prevents users from importing files from outside of src/ (or node_modules/)
      new ModuleScopePlugin(path.resolve(__dirname, 'src')),
    ],
  },

  plugins: [

    // Define process.env variables
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'development',
    }),

    // Define global variables
    new webpack.DefinePlugin({
      DEBUG: DEV,
      PUBLIC_URL: JSON.stringify(PUBLIC_URL),
    }),

    new InterpolateHtmlPlugin({ PUBLIC_URL }),
    
    // HTML pages
    ...['index.html'].map(
      page =>
        new HtmlWebpackPlugin({
          template: page,
          filename: page,
          inject: false,
          minify: false,
        })
    ),
    
    // DEV-only plugins
    ...(DEV
      ? [
          // Enable source maps
          new webpack.SourceMapDevToolPlugin(),
          // Add module names to factory functions so they appear in browser profiler
          new webpack.NamedModulesPlugin(),
          // This is necessary to emit hot updates
          new webpack.HotModuleReplacementPlugin(),
          // Watcher doesn't work well if you mistype casing in a path so we use
          // a plugin that prints an error when you attempt to do this
          new CaseSensitivePathsPlugin(),
          // If you require a missing module and then `npm install` it, you still have
          // to restart the development server for Webpack to discover it. This plugin
          // makes the discovery automatic so you don't have to restart
          new WatchMissingNodeModulesPlugin(
            path.resolve(__dirname, 'node_modules')
          ),
        ]
      : []),
    
    // PROD-only plugins
    ...(PROD
      ? [
          new BundleAnalyzerPlugin({ analyzerMode: 'static' }),
          // Concatenate the scope of all modules (less wrapper functions)
          new webpack.optimize.ModuleConcatenationPlugin(),
          // Optimize hashes
          new webpack.HashedModuleIdsPlugin(),
          new WebpackChunkHash(),
          // Minify the code.
          new webpack.optimize.UglifyJsPlugin({
            compress: {
              warnings: false,
              comparisons: false,
            },
            output: {
              comments: false,
              ascii_only: true,
            },
          }),
          // Copy statid dir to build
          new CopyWebpackPlugin([path.resolve(__dirname, 'static')]),
        ]
      : []),
  ],

  module: {
    strictExportPresence: true,
    rules: [
      {
        oneOf: [
          {
            test: /\.js$/i,
            loader: 'babel-loader',
            options: { cacheDirectory: true },
          },
          {
            test: /\.css$/i,
            exclude: /node_modules/,
            use: [
              { loader: 'style-loader', options: { sourceMap: true } },
              { loader: 'css-loader', options: { sourceMap: true, importLoaders: 1 } },
              { loader: 'postcss-loader', options: { sourceMap: true } },
            ],
          },
          {
            exclude: /\.(js|html|json)$/i,
            loader: 'file-loader',
            options: { name: 'assets/[name].[ext]' },
          },
        ],
      },
    ],
  },

  devServer: {
    contentBase: path.join(__dirname, 'static'),
    watchContentBase: false,
    compress: true,
    host: '0.0.0.0',
    hot: true,
    disableHostCheck: true
  },

  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },

  performance: PROD ? { hints: 'warning' } : false,

};
